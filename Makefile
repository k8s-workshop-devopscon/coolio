###
#  general configuration of the makefile itself
###
SHELL := /bin/bash
.DEFAULT_GOAL := help
.PHONY: help

ORG ?= oz123#? Organizaiton name in gitlab\docker hub
APP ?= coolio#? The docker image name 
VERSION ?= $(shell git describe --always)#? detemine the application version


define PRINT_HELP_PYSCRIPT
import re, sys
from collections import defaultdict

targets = {}
variables = []
local_variables = defaultdict(list)

print("Targets:\n")
for line in sys.stdin:
    match = re.search(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
    if match:
        target, help = match.groups()
        targets.update({target: help})
    local_var = re.search(r'^(?P<target>[\w_\-/]+):.(?P<var>[A-Z_]+).*#\? (?P<help>.*)$$', line)
    if local_var:
        gd = local_var.groupdict()
        local_variables[gd['target']].append({'name': gd['var'], 'help': gd['help']})
        continue
    vars = re.search(r'([A-Z_]+).*#\? (.*)$$', line)
    if vars:
        target, help = vars.groups()
        variables.append("%-20s %s" % (target, help))

for target, help in targets.items():
    print("%-20s %s\n" % (target, help))
    if target in local_variables:
        [print("  %s - %s " % (i['name'], i['help'])) for i in local_variables[target]]
    print("")
if variables:
    print("Global Variables you can override:\n")
    print('\n'.join(variables))

endef

export PRINT_HELP_PYSCRIPT


help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)	

deploy:  ## deploy the software to kubernetes
	@kubectl get pod

build: BUILDER := img #? Which OCI image builder to use (e.g. docker, podman ...)
build: ## build the software
	@echo "building..."
	$(BUILDER) build -t $(ORG)/$(APP):$(VERSION) .

push: BUILDER := img #? Which OCI image builder to use (e.g. docker, podman ...)
push: ## build the software
	@echo "building..."
	$(BUILDER) push $(ORG)/$(APP):$(VERSION)

test: unittest e2e-test  ## run the tests
	@echo "tests"

unittest: HOST := localhost
unittest: PORT := 80
unittest: ## run unittest
	@echo "unittest"
	curl -s $(HOST):$(PORT) | grep -qc "background: red;" || echo "OK, not red"
	curl -s $(HOST):$(PORT) | grep -qc "background: purple;" || echo "OK, not purple"
	curl -s $(HOST):$(PORT) | grep -qc "background: black;" || echo "OK, not black"
	curl -s $(HOST):$(PORT) | grep -qc "background: green;"


e2e-test: ## run e2e-test
	@echo "e2e-test"


deploy:
	kubectl apply -f k8s/deployment.yaml
